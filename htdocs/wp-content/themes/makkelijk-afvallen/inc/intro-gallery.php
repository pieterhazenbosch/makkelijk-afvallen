<?php
$intro_sectie = get_field('intro_sectie');
if( $intro_sectie  ): ?>
<ul class="intro-gallery" style="background-image: url('<?php echo esc_url( $intro_sectie ['galerij_afbeelding_3']['url'] ); ?>');">
    <li><img src="<?php echo esc_url( $intro_sectie ['galerij_afbeelding_1']['url'] ); ?>" alt="<?php echo esc_attr( $intro_sectie['galerij_afbeelding_1']['alt'] ); ?>"></li>
    <li><img src="<?php echo esc_url( $intro_sectie ['galerij_afbeelding_2']['url'] ); ?>" alt="<?php echo esc_attr( $intro_sectie['galerij_afbeelding_2']['alt'] ); ?>"></li>
    <li><img src="<?php echo esc_url( $intro_sectie ['galerij_afbeelding_4']['url'] ); ?>" alt="<?php echo esc_attr( $intro_sectie['galerij_afbeelding_4']['alt'] ); ?>"></li>
    <li><img src="<?php echo esc_url( $intro_sectie ['galerij_afbeelding_5']['url'] ); ?>" alt="<?php echo esc_attr( $intro_sectie['galerij_afbeelding_5']['alt'] ); ?>"></li>
</ul>
<?php endif; ?> 