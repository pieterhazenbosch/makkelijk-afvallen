        <footer>
        <div class="container">
            <div class="row py-lg-5 pt-5">
                <div class="col-lg-3 px-lg-0">
                    <h4><?php _e( 'Makkelijk Afvallen', 'makkelijk-afvallen' ); ?></h4>
                    <ul>
                        <li><a href="#"><?php _e( 'Succesverhalen', 'makkelijk-afvallen' ); ?></a></li>
                        <li><a href="#"><?php _e( 'Over ons', 'makkelijk-afvallen' ); ?></a></li>
                        <li><a href="#"><?php _e( 'Contact', 'makkelijk-afvallen' ); ?></a></li>
                    </ul>
                </div>
                <div class="col-lg-3">
                    <h4><?php _e( 'Lorem Ipsum', 'makkelijk-afvallen' ); ?></h4>
                    <ul>
                        <li><a href="#"><?php _e( 'Shop', 'makkelijk-afvallen' ); ?></a></li>
                        <li><a href="#"><?php _e( 'Recepten', 'makkelijk-afvallen' ); ?></a></li>
                        <li><a href="#"><?php _e( 'Artikelen', 'makkelijk-afvallen' ); ?></a></li>
                    </ul>
                </div>
                <div class="col-lg-3">
                    <h4><?php _e( 'Vivamus lacinia', 'makkelijk-afvallen' ); ?></h4>
                    <ul>
                        <li><a href="#"><?php _e( 'Cras at tincidunt', 'makkelijk-afvallen' ); ?></a></li>
                        <li><a href="#"><?php _e( 'Sit amet', 'makkelijk-afvallen' ); ?></a></li>
                    </ul>
                </div>
                <div class="col-lg-3 px-lg-0">
                    <div class="social">
                    <h4><?php _e( 'Volg ons', 'makkelijk-afvallen' ); ?></h4>
                    <ul class="social-icons">
                        <li class="mb-2"><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/img/icons/facebook.svg" alt="Facebook logo"></a></li>
                        <li class="mb-2"><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/img/icons/twitter.svg" alt="Twitter logo"></a></li>
                        <li class="mb-2"><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/img/icons/instagram.svg" alt="Instagram logo"></a></li>
                        <li class="mb-2"><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/img/icons/youtube.svg" alt="Youtube logo"></a></li>
                    </ul>
                </div>
            </div>
            </div>
            <div class="row">
                <div class="sub col">
                    <ul>
                        <li><a href="#"><?php _e( 'Algemene voorwaarden', 'makkelijk-afvallen' ); ?></a></li>
                        <li><a href="#"><?php _e( 'Disclaimer', 'makkelijk-afvallen' ); ?></a></li>
                        <li><a href="#"><?php _e( 'Privacy-en cookiebeleid', 'makkelijk-afvallen' ); ?></a></li>
                    </ul>
                    <span><?php _e( 'Copyright © 2014 - 2019 Makkelijk Afvallen', 'makkelijk-afvallen' ); ?></span>
                </div>
            </div>
        </div>
        </footer>
        <?php wp_footer(); ?>
    </body>
</html>