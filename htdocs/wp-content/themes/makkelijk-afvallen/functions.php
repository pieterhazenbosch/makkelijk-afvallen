<?php 

// Adding the CSS and JS files
function ma_setup_styles() {

    wp_enqueue_style( 'ma-styles', get_template_directory_uri() . '/css/style.css', array(), '' );
}
add_action('wp_enqueue_scripts', 'ma_setup_styles');

function add_google_fonts() {
 
    wp_enqueue_style( 'wpb-google-fonts', 'https://fonts.googleapis.com/css?family=Muli:300,400,600,700&display=swap', true ); 
} 
add_action( 'wp_enqueue_scripts', 'add_google_fonts' );

function add_bootstrap() {
    wp_enqueue_script( 'boot1','https://code.jquery.com/jquery-3.3.1.slim.min.js', array( 'jquery' ),'',true );
    wp_enqueue_script( 'boot2','https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js', array( 'jquery' ),'',true );
    wp_enqueue_script( 'boot3','https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js', array( 'jquery' ),'',true );
}
add_action( 'wp_enqueue_scripts', 'add_bootstrap' );

function ma_setup_scripts() {

    wp_enqueue_script( 'ma-scripts', get_template_directory_uri() . '/scripts/main.js', array( 'jquery' ), '', true );
}
add_action('wp_footer', 'ma_setup_scripts');


