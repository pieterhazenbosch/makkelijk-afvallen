var gulp = require('gulp');
var sass = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');
var cleanCSS = require('gulp-clean-css');
var autoprefixer = require('gulp-autoprefixer');
var plumber = require('gulp-plumber');
var jshint = require('gulp-jshint');
var uglify = require('gulp-uglify');
var concat = require('gulp-concat');
var rename = require("gulp-rename");
var stylish = require('jshint-stylish');
var imagemin = require('gulp-imagemin');
var cache = require('gulp-cache');
var runSequence = require('run-sequence');
var browserSync = require('browser-sync').create(); // Only for front-end

// Sass
gulp.task('sass', function() {
    return gulp.src('src/scss/style.scss')
    .pipe(sourcemaps.init())
    .pipe(plumber())
    .pipe(sass.sync().on('error', sass.logError))
    .pipe(autoprefixer({
        browsers: ['> 5%', 'last 2 versions']
    }))
    .pipe(cleanCSS())
    .pipe(sourcemaps.write())
    .pipe(gulp.dest('css'))
    .pipe(browserSync.stream());
});

// Javascript
gulp.task('js', ['js-hint', 'js-vendor'], function(){
    return gulp.src('src/scripts/*.js')
    .pipe(concat('main.js'))
    .pipe(gulp.dest('scripts'))
});

// Javascript js-hint
gulp.task('js-hint', function() {
    return gulp.src('src/scripts/*.js')
    .pipe(jshint())
    .pipe(jshint.reporter('jshint-stylish'))
    .pipe(jshint.reporter('fail'))
});

// Javascript plugin
gulp.task('js-vendor', function() {
    return gulp.src('src/scripts/vendor/*.js')
    .pipe(gulp.dest('scripts/vendor'))
});

// Images
gulp.task('images', function(){
    return gulp.src('src/img/**/*.+(png|jpg|jpeg|gif|svg)')
        // Caching images that ran through imagemin
        .pipe(cache(imagemin({
        interlaced: true
    })))
    .pipe(gulp.dest('img'))
});

// Video
gulp.task('videos', function() {
    return gulp.src('src/img/**/*.mp4')
    .pipe(gulp.dest('img'))
});

// Downloads
gulp.task('downloads', function() {
    return gulp.src('src/downloads/**/*')
    .pipe(gulp.dest('downloads'))
});

// Fonts
gulp.task('fonts', function() {
    return gulp.src('src/fonts/**/*')
    .pipe(gulp.dest('fonts'))
});

// Build
gulp.task('build', function (done) {
    runSequence([
        'sass',
        'js',
        'images',
        'videos',
        'downloads',
        'fonts'
    ], done)
});

gulp.task('watch', function () {
    gulp.watch('src/scss/**/*.scss', ['sass']);
    gulp.watch('src/img/**', ['images']);
    gulp.watch('src/img/**', ['videos']);
    gulp.watch('src/scripts/**/*.js', ['js']);
});

gulp.task('default', ['build', 'watch'], function () {
    // Put everything under watch? Or make a separated "build" command?
});