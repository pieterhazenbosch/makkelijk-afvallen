<?php
get_header(); 
?>

<section class="home-intro">
    <div class="container">
        <div class="row">
            <div class="col-lg-6">
                <?php get_template_part('inc/intro', 'gallery') ?>
            </div>
            <div class="col-lg-6">
                <div class="intro-content">
                    <h1><?php echo get_field('intro_sectie')['titel']; ?></h1>
                    <p><?php echo get_field('intro_sectie')['beschrijving']; ?></p>
                    <ul class="category-buttons">
                        <li><a href="<?php echo site_url('/weekmenus') ?>"><?php _e( 'Weekmenu’s', 'makkelijk-afvallen' ); ?></a></li>
                        <li><a href="<?php echo site_url('/recepten') ?>"><?php _e( 'Recepten', 'makkelijk-afvallen' ); ?></a></li>
                        <li><a href="<?php echo site_url('/inspiratie') ?>"><?php _e( 'Inspiratie', 'makkelijk-afvallen' ); ?></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="cta-1">
    <div class="container">
        <div class="row">
            <div class="col-lg-6">
                <div class="content">
                    <h2>Geniet van onze lekkere weekmenu’s en val af.</h2>
                    <p>Weekmenu’s voor 50 dagen, maakt afvallen makkelijk. Volg onze heerlijke koolhydraatarme weekmenu’s en val af terwijl je blijft genieten van lekker eten.</p>
                    <p class="mb-6">Met de meegeleverde boodschappenlijstjes hoef je ook in de supermarket niet meer na te denken over wat je nu weer moet kopen.</p>
                    <a href="#" class="button button-outline">Meer over onze weekly mealplans 
                        <svg width="7" height="12" viewBox="0 0 7 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M6.74972 5.99998C6.74992 6.16352 6.71524 6.32523 6.64799 6.4743C6.58074 6.62338 6.48246 6.7564 6.35972 6.86448L0.78372 11.77C0.598945 11.925 0.360843 12.0016 0.120326 11.9834C-0.120191 11.9652 -0.344033 11.8536 -0.503335 11.6725C-0.662637 11.4913 -0.744756 11.2551 -0.732132 11.0142C-0.719507 10.7733 -0.613142 10.547 -0.43578 10.3835L4.44022 6.09398C4.4536 6.08225 4.46433 6.06779 4.47168 6.05158C4.47902 6.03537 4.48282 6.01778 4.48282 5.99998C4.48282 5.98218 4.47902 5.96459 4.47168 5.94838C4.46433 5.93216 4.4536 5.91771 4.44022 5.90598L-0.43578 1.61648C-0.613142 1.45301 -0.719507 1.22663 -0.732132 0.985752C-0.744756 0.744876 -0.662637 0.50862 -0.503335 0.327503C-0.344033 0.146386 -0.120191 0.0347826 0.120326 0.0165578C0.360843 -0.00166706 0.598945 0.0749328 0.78372 0.229978L6.35772 5.13398C6.48076 5.24225 6.57935 5.37547 6.64694 5.52478C6.71453 5.67409 6.74956 5.83608 6.74972 5.99998Z" fill="#FA585D"/>
                        </svg>
                    </a>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="image">
                    <img src="img/recipe-book-big.png" alt="Recipe book">
                    <a href="#" class="button button-outline">Meer over onze weekly mealplans 
                        <svg width="7" height="12" viewBox="0 0 7 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                           <path d="M6.74972 5.99998C6.74992 6.16352 6.71524 6.32523 6.64799 6.4743C6.58074 6.62338 6.48246 6.7564 6.35972 6.86448L0.78372 11.77C0.598945 11.925 0.360843 12.0016 0.120326 11.9834C-0.120191 11.9652 -0.344033 11.8536 -0.503335 11.6725C-0.662637 11.4913 -0.744756 11.2551 -0.732132 11.0142C-0.719507 10.7733 -0.613142 10.547 -0.43578 10.3835L4.44022 6.09398C4.4536 6.08225 4.46433 6.06779 4.47168 6.05158C4.47902 6.03537 4.48282 6.01778 4.48282 5.99998C4.48282 5.98218 4.47902 5.96459 4.47168 5.94838C4.46433 5.93216 4.4536 5.91771 4.44022 5.90598L-0.43578 1.61648C-0.613142 1.45301 -0.719507 1.22663 -0.732132 0.985752C-0.744756 0.744876 -0.662637 0.50862 -0.503335 0.327503C-0.344033 0.146386 -0.120191 0.0347826 0.120326 0.0165578C0.360843 -0.00166706 0.598945 0.0749328 0.78372 0.229978L6.35772 5.13398C6.48076 5.24225 6.57935 5.37547 6.64694 5.52478C6.71453 5.67409 6.74956 5.83608 6.74972 5.99998Z" fill="#FA585D"/>
                        </svg>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>

<?php
get_footer();
?>